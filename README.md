# Drupal Security 101



## [Setup] - Workshop requirements

The following tools are required for this workshop:
* [Git](https://git-scm.com/)
* [Docker](https://docs.docker.com/engine/install/)
* [DDEV](https://ddev.readthedocs.io/en/latest/users/install/ddev-installation/)

Alternatively, please check [Drupal official system requirements](https://www.drupal.org/docs/getting-started/system-requirements/overview) to setup the local environment or use [Lando](https://docs.lando.dev/getting-started/installation.html) and setup Drupal normally.

This workshop's slide deck is available [here](https://docs.google.com/presentation/d/1gk8N8v3fi1MgOSgrBodlBupS7SSCtM3pxCu2FEmRZek/edit?usp=sharing).

## Getting started
### Retrieve code
Clone the git repository and go in the project folder.

```shell
git clone git@gitlab.com:nicoloye/drupal-security-101.git
cd drupal-security-101
```

### Start the local environment

```shell
ddev start
ddev composer install
ddev ssh
```

### Install the website and connect as admin.

Once the website is installed, click on the one-time login link provided by the last command.
```shell
drush si demo_umami --account-name=admin --account-pass=admin -y
drush uli
```

## [Workshop] - Drupal security 101

### Basics

More about the Common Misuse Scoring System [here](https://www.drupal.org/drupal-security-team/security-risk-levels-defined).

More detail about Drupal community security releases [here](https://www.drupal.org/drupal-security-team/security-release-numbers-and-release-timing#s-release-timing)

### Configurations

#### Files & configs

##### 1. .htaccess files

Beware when not using Apache.
Beware on hosting providers / PaaS.

For local environments, on DDEV check the nginx configuration file here: `.ddev/nginx_full/nginx-site.conf`.
Lando works on apache by default.

##### 2.  Configurations & services outside of docroot

Create a folder for your drupal configuration outside the docroot.

```shell
mkdir config/drupal/ -p
```

Move your configuration folder to this new path.

```shell
mv web/sites/default/files/sync/ config/drupal/
```

Create a new settings file in this new path (for this example we will just copy the one provided by DDEV).
```shell
mv web/sites/default/settings.ddev.php config/drupal/settings.app.php
```

Edit `web/sites/default/settings.php`.

Add this code at the end of the file:
```php
if (file_exists($app_root . '/../config/drupal/settings.app.php')) {
  include $app_root . '/../config/drupal/settings.app.php';
}
```

Edit `config/drupal/settings.app.php` and replace the `$settings['config_sync_directory']` definition with the following one:
```php
  $settings['config_sync_directory'] = $app_root . '/../config/drupal/sync';
```

Now our configuration directory is outside the docroot and properly configured for Drupal to find it.

Ideally you can try this manipulation (but it can by tricky to handle.)

Rename your current docroot directory to /app.
```shell
mv web app
```
Update the composer.json of your root package with the following values:

```json
"extra": {
    "drupal-paranoia": {
        "app-dir": "app",
        "web-dir": "web"
    },
    "installer-paths": {
        "app/core": ["type:drupal-core"],
        "app/libraries/{$name}": ["type:drupal-library"],
        "app/modules/contrib/{$name}": ["type:drupal-module"],
        "app/profiles/contrib/{$name}": ["type:drupal-profile"],
        "app/themes/contrib/{$name}": ["type:drupal-theme"],
        "drush/contrib/{$name}": ["type:drupal-drush"]
    }
}
```
Be sure to replace all the installer-paths definitions to avoid any issue with drush or composer.

Install the drupal-paranoia composer plugin.
```shell
composer require drupal-composer/drupal-paranoia:~1
```

All the risky assets are automatically symlinked from `/app` to `/web` folder.
More detail [here](https://github.com/drupal-composer/drupal-paranoia).

##### 3.  Proper configuration & services loaded on production

Check in the `/sites`, `/sites/default` and `/config/drupal` folders.
Be sure to check that there is no `settings.local.php`, `development.services.yml`
or any non-production settings and services loaded.

##### 4.  Production configuration is the default one

Be sure that `/config/drupal/sync` only contains production ready condigurations.
To add overrides for specific environments (dev modules, specific functionnalities, etc),
please consider using the [Config split](https://www.drupal.org/project/config_split) module
following one of the [recommended folder structures](https://www.drupal.org/docs/contributed-modules/configuration-split/split-directory-structure).

##### 5. Do not export / version sensitive values

Store placeholders in the Drupal configuration.
So that no yml file in `/config/drupal/sync` contains any sensitive value.

Override the values from the settings file with the [configuration override system](https://www.drupal.org/docs/drupal-apis/configuration-api/configuration-override-system).

Let's try with the website main email address.

Go to `/admin/config/system/site-information` and type `mail@placeholder.com` in the `Email address` field.

Now let's create an environment variable for the real value.
Create a .env file in `.ddev/`, edit it with the following line and save it.

```shell
SITE_MAIL='real@email.com'
```

Restart DDEV to load this new variable:

```shell
ddev restart
```

Finally edit `config/drupal/settings.app.php` to add the following configuration override:

```php
$config['system.site']['mail'] = $_ENV['SITE_MAIL'];
```

You can test that your override is properly taken into account with the following commands:

```shell
drush cget system.site mail
drush cget system.site mail --include-overridden
```

##### 6. Secured storage for sensitive values

Do not store any sensitive value as a raw value, even in the settings file.
Please use a solution to store the value properly:
* Use environment variables through the [dotenv module](https://www.drupal.org/project/dotenv) (DO NOT VERSION THE .ENV FILE!) or a generic .env file.
* Use environment variables through secrets defined in your CI, for example in [Gitlab CI](https://docs.gitlab.com/ee/ci/secrets/).
* Use an encryption method to store any sensitive data, such as the [Key](https://www.drupal.org/project/key) or [Vault](https://www.drupal.org/project/vault) modules.

Be sure to use [scripts in your CI](https://geekflare.com/github-credentials-scanner/) to detect secrets that may have already been versioned.

##### 7. Restrictive trusted host patterns

Edit `/config/drupal/settings.app.php` and search for this definition: `$settings['trusted_host_patterns']`.
Be sure that the regex here is as restrictive as possible. The more strict, the better.

Modify your local entry to match your local environment url:
```php
$trusted_host_patterns = [
 '^drupal-security-101\.ddev\.site$',
];
```

#### Folders

##### 1. Media management

Go to `/admin/config/media/file-system` and be sure to select the appropriate setting for `Default download method`.
Be aware that the private file system has a performance impact as it checks the user permission for each requested file.

##### 2. Private files outside docroot

Non-public files should be stored in the private storage system.
Edit `/config/drupal/settings.app.php` and add the following line at the end of the file.

```php
$settings['file_private_path'] = $app_root . '/../data/private';
```

Clear the caches.

```shell
drush cr
```

In `/data/private` check that a .htaccess file is present (should be automatically generated) with the `Deny from all` policy.

##### 3. Temporary files folder outside docroot

Go to `/admin/config/media/file-system` and check that the `Temporary directory` setting defines a path outside the docroot.
This setting can be modified with a similar definition in `/config/drupal/settings.app.php`.

```php
$settings['file_temp_path'] = '/tmp';
```

##### 4. No versioned dump

Use scripts in your CI to be sure no database dump has been versioned.
Be sure to setup exceptions has Drupal core is delivered with harmless dumps for test purposes.
If your technical requirements let you no choice but to version a database dump, be sure to:
* Store it outside docroot.
* Sanitize it from any personal data (use placeholders for any business case data you may require.)

#### Security policies

##### 1. HTTPS everywhere!

Configure your servers to [deliver HTTPS](https://www.drupal.org/https-information).
Provide HTTPS redirections and be sure that no mixed contents are delivered.

##### 2. HSTS HTTP header

HSTS stands for HTTP Strict Transport Security.
This header ensure to force the browser to reach all resources of the application through HTTPS.
This is more secure than configuring redirections.
Like any other HTTP header this can be set on server side. More details on this header [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security).

In Drupal this header can be set through the seckit module, to install it run the following commands:

```shell
composer require drupal/seckit
drush en seckit -y
```

Go to `/admin/config/system/seckit` and open the `SSL/TLS` section:
* Check the `HTTP Strict Transport Security` option.
* Set the max-age value to 31536000 (1 year.)
* Check `Include Subdomains` if you want to secure any subdomain too.
* The preload option require you to register the website domain on an HSTS preload service for it to be hardcoded in any modern browser. More details about it [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security#preloading_strict_transport_security).

Save the form to set the header on the website.

##### 3. CORS

CORS stands for Cross-Origin Resource Sharing.
This header allows the server to speciy any origin other than its own a browser should permit loarding resources from.
Like any other HTTP header this can be set on server side. More details on this header [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS).

On Drupal this is a native feature through the `services.yml` files.
Let's create one!

```shell
cp web/sites/default/default.services.yml config/drupal/app.services.yml
```

Add this line in `/config/drupal/settings.app.php`:

```php
$settings['container_yamls'][] = $app_root . '/../config/drupal/app.services.yml';
```

Edit `config/drupal/app.services.yml` and find `cors.config` definition to define your CORS:
* Set `enabled` to `true`
* Set `allowedHeaders` to define all HTTP headers allowed, note that CORS already handle a safelist of header, more details about this [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Headers).
* Set `allowedMethods` to define all HTTP method allowed, more details about this [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Methods).
* Set `allowedOrigins` to define all allowed origins other than the server own one, more details about this [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Origin).
* Set `exposedHeaders` to define all HTTP headers that should be exposed to any script running on the website. Use with caution. More details about this [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Expose-Headers).
* Set `maxAge` to define how long a browser should cache the content of `Access-Control-Allow-Methods` and `Access-Control-Allow-Headers` headers. More details about this [here]().
* Set `supportsCredentials` to define if credentials (cookies, authorization headers or TLS certificates) are exposed to any script running on the website in a certain context. More details about this [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Credentials).

Once your modification done, clear the cache and check the CORS apply correctly.

```shell
drush cr
```

##### 4. CSP

CSP stands for Content Security Policy.
This header helps to detect and mitigate certain types of attacks.
Like any other HTTP header this can be set on server side. More details on this header [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP).

On Drupal this can be achieved with two modules but we aware that they don't work well together.
Be sure to set a CSP only with one of them.

With Seckit (see above for installation), go to `/admin/config/system/seckit` and open the `Send HTTP response header` section:
* Enable the CSP header by checking `Send HTTP response header`
* Set values to the directives required. Be sure to includes single quotes to all values.

With CSP, run the followin commands:

```shell
composer require drupal/csp
drush en csp -y
```

Go to `/admin/config/system/csp` and check the configuration:
* Check `Enable 'Enforced'` to enable the CSP
* Check any directive to configure it.

Ideally set the `default-src` directive to `'none'` and iterate on all blocked resources to allow them one after the other.
You can find more information about the directives starting on [this page](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/Sources) and following the menu on the left for each other directive.

##### 5. X-FRAME-OPTIONS HTTP header

This header indicates to the browser if a content can be rendered in a `<frame>`, `<iframe>`, `<embed>` or `<object>` element.
Like any other HTTP header this can be set on server side. More details about this [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options).

This can be configured in seckit (see above for installation), go to `/admin/config/system/seckit` and open the `X-Frame-Options header` section:
* By default is set to `sameorigin`
* You can also add `allow-from` directive. This is an obsolete directive that no longer works in modern browsers.

This header is globally obsolete and should be replaced by the appropriate directives in a CSP header.

##### 6. Referrer Policy

This header controls how much of the referrer information is shared with requests.
The less information shared, the better.
Like any other HTTP header this can be set on server side. More details about this [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy).

Again, this can be configured in seckit (see above for installation), go to `/admin/config/system/seckit` and open the `Miscellaneous` section:
* Check `Referrer-Policy` to enable it
* Then select a policy. The default one is `no-referrer-when-downgrade` which prevent sharing any information when navigating from HTTP to HTTPS.

This header protects the privacy of the users and should be considered to be as restrictive as possible.

Note that this feature should be available in core soon, please follow [this issue](https://www.drupal.org/project/drupal/issues/3027122) for more information.

##### 7. SRI

SRI stands for Subresource Integrity.
This system allows the browser to check the integrity of any file downloaded to prevent its execution if something is not as expected.
This prevents man-in-the-middle attacks.
More details about this [here](https://developer.mozilla.org/en-US/docs/Web/Security/Subresource_Integrity).

This feature can already be handled in core through libraries declarations:

```yml
my-lib/styles.css: {
 type: "external",
 attributes: {
   integrity: "sha384-foo1c6b",
   crossorigin: "anonymous"
 }
}
```

In this case, the integrity hash needs to be updated any time the resource is updated.

Note that this should be automatically calculated form aggregated assets in core soon, please follow [this issue](https://www.drupal.org/project/drupal/issues/3304450) for more information.

##### 8. X-CONTENT-TYPE-OPTIONS HTTP header

This header is a marker to indicate that the MIME types advertised in the `Content-Type` headers should be followed and not be changed.
Like any other HTTP header this can be set on server side. More details about this [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options).

This header is automatically generated by Drupal core. Just remember that it exists.

##### 9. X-XSS-PROTECTION HTTP header

This header is obsolete and only available for old browsers that does not support the CSP header.
Like any other HTTP header this can be set on server side. More details about this [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-XSS-Protection).

Again, this can be configured in seckit (see above for installation), go to `/admin/config/system/seckit` and open the `X-XSS-Protection header` section:
* Change the value to add the header to your pages.

Be careful using this header as it can cause vulnerabilies in certain contexts. Secured values are `0` or `1; mode=block`. More information about thi [here](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-XSS-Protection#vulnerabilities_caused_by_xss_filtering).

##### 10. Remove Drupal signature headers

Drupal ships with a bunch of signature headers that can be removed to obfuscate that your website is running on it.
This in itself is not enough to secure the website, but combined with the previous methods it can add a layer of complexity for some attackers.

Install `Remove HTTP headers` module by running the following commands:

```shell
composer require drupal/remove_http_headers
drush en remove_http_headers -y
```

Then go to `/admin/config/system/remove-http-headers` and configure any additional headers you way want to remove.

#### Users

##### 1. A single user account for each individual

Be sure to create an account for each and every individual.
A shared account is a vulnerability as it increases the risk for credentials to be leaked,
and it prevents any proper log analysis to know who did what on the application.

##### 2. Block user 1 account (on production)

The super admin user is not really to be considered as a user.
Permissions don't apply to it, and it has multiple bypasses for certain logic in the core.
Its usage should be restricted to very specific use cases where no other administrator accounts can be used.
Consider blocking it, at least on production, to prevent any problem.

To do so you can either:

* Edit the user 1 entity on `/user/1/edit` while being connected as an authorized user to set its status to `Blocked`.
* Run the following drush command:

```shell
drush ublk --uid=1
```

##### 3. Password policy & other login securities

The security of your application depends also on how secured your user access means will be.
Trying to enforce a too strict password policy [will be counter-effective](https://www.starlab.io/blog/why-enforced-password-complexity-is-worse-for-security-and-what-to-do-about-it).

Be cautious when defining one. Be sure to define a coherent policy that is up-to-date with the current state of the art.
Here is an [example from Acquia](https://dev.acquia.com/blog/password-policies-and-drupal) that can serve as a base but will need updates for your specific cases.

Security experts recommend:
* Propose the usage of [passphrases](https://www.techtarget.com/searchsecurity/definition/passphrase) instead of passwords.
* Encourage the usage of password managers
* Permit reasonably long passwords
* Enable Multi-factor authentication everywhere
* Implement any other [security alternative](https://itsecuritywire.com/featured/towards-a-password-less-future-better-security-alternatives-to-password/)

To add a password policy, run the following commands:

```shell
composer require drupal/password_policy
drush en password_policy -y
```
Then go to `/admin/config/security/password-policy` to set your policies.

To add a two-factor authentication, run the following commands:

```shell
composer require drupal/tfa
drush en tfa -y
```

Then follow the [README file instructions](https://git.drupalcode.org/project/tfa/-/blob/8.x-1.x/README.md) to setup the different providers for your application.

##### 4. Who can create new user accounts?

Check that the user creation policy complies with business needs.

Go to `/admin/config/people/accounts` and configure the user permissions according to your needs.

##### 5. Check user / roles permissions

“Not enough” is better than “too much”.
Beware of the anonymous role.

Go to `/admin/people/permissions` and configure the user permissions according to your needs.

##### 6. Search API indexes

Search API is a common module used for search features on Drupal.
When defining an index on this module, be sure that the `Content Access` processor is always enabled so that user permissions are properly indexed with every content.

#### Errors & logs

##### 1. Hide error messages

The less information is given to any malicious user, the better it is.
All error messages should be hidden by default.

Go to `/admin/config/development/logging` and ensure `Error messages to display` is set to `None`.

A better way to control that is to edit `/config/drupal/settings.app.php` and set this definition at the end of the file:

```php
$config['system.logging']['error_level'] = 'hide';
```

##### 2. Configure logs

Even when hidden, all warnings and errors should be logged for proper analysis.
The data retention period should be defined with a consistent value compatible with the business requirements.

Go to `/admin/config/development/logging` and be sure to set `Database log messages to keep` to a proper value (for database log only).

Ideally disable DBlog, favor Syslog or the [Monolog](https://www.drupal.org/project/monolog) modules.

```shell
drush pmu dblog -y
drush en syslog -y
```

Go to `/admin/config/development/logging` to configure how the syslog will retrieve the application logs.

Alternatively you can use third party logging solution such as [Sentry](https://www.drupal.org/project/sentry_io) (maintainship open for this module).

#### Core & modules

##### 1. Up-to-date security updates & status report

Regularly check status with Update Manager.
Run the following commands to enable the module:

```shell
drush en update -y
```

Then go to `/admin/reports/updates` to check the core and contrib statuses.
This module should not be left enabled on production, be sure to use it only for quick audits.

##### 2. Modules / dev dependencies at the right place

Be sure that all package managers declare their dev dependencies properly.
No dev dependency should be declared in the main dependency context.

For example with composer, check the `composer.json` file:
```json
"require-dev": {
   "behat/mink": "^1.8",
   "behat/mink-goutte-driver": "^1.2",
   "drupal/devel": "^4.1",
   "drupal/stage_file_proxy": "^1.1",
   "phpunit/phpunit": "^9.5"
},
```

Or with npm, check the `package.json` file:
```json
"devDependencies" : {
  "browser-sync": "^2.18.12",
  "node-normalize-scss": "^3.0.0",
  "stylelint": "^7.12.0",
  "stylelint-config-standard": "^16.0.0",
  "stylelint-order": "^0.5.0"
}
```

##### 3. No unused module or unused technical element

Be sure that no dev dependencies are installed on production environment.
For example, when using a CI to deploy Drupal on production, run this command on installation:

```shell
composer install --no-dev
```

Or for a theme built with npm:

```shell
npm install --omit=dev
```

This applies to any package manager and technical element.

##### 4. No obsolete components

To be sure you don't add components with security vulnerabilities.
If you're using an old version of Composer (<2.4), consider installing the composer security advisories plugin

```shell
composer require --dev roave/security-advisories:dev-latest
```

With newer versions of Composer be sure to set the `audit.abandoned` configuration to `fail` (will be the default value starting Composer 2.7).

```shell
composer config audit.abandoned fail
```

Doing so you can scan any existing component in an automated CI for example:

```shell
composer audit
```

Or any new component added, like if you try to add this module, a warning should occur.

```shell
composer require drupal/xsendfile:1.1.0
```

Alternatively you can use third party checkers like [Dependabot](https://github.com/dependabot/dependabot-core) or [Renovate](https://github.com/marketplace/renovate).

### Resources

This workshop's slide deck is available [here](https://docs.google.com/presentation/d/1gk8N8v3fi1MgOSgrBodlBupS7SSCtM3pxCu2FEmRZek/edit?usp=sharing).
You'll find many useful resources about security in general in them.

## Thanks!

Thank you for attending this workshop.
Comments and critics are welcome, feel free to contact me:
* [Twitter](https://twitter.com/nicoloye)
* [LinkedIn](https://www.linkedin.com/in/nicolas-loye/)
* [Drupal](https://www.drupal.org/u/nicoloye)
